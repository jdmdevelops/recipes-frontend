import './App.css'
import { useEffect, useState } from 'react'

const Create = (props) => {
  const [recipe, setRecipe] = useState({
    name: '',
    image: '',
    ingredients: '',
    steps: '',
  })

  const handleInputChange = (e) => {
    setRecipe({ ...recipe, [e.target.name]: e.target.value })
  }

  const handleCreate = () => {
    let recipeCopy = { ...recipe }

    recipeCopy.ingredients = recipeCopy.ingredients
      .split(',')
      .map((str) => str.trim())
    recipeCopy.steps = recipeCopy.steps.split(',').map((str) => str.trim())

    props.setRecipes([...props.recipes, recipeCopy])

    const request = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(recipeCopy),
    }
    fetch('http://localhost:8080/recipe', request)
      .then((res) => res.text())
      .then((data) => console.log(data))
  }

  return (
    <form>
      <legend>Create Recipe</legend>
      <label>Name: </label>
      <input type="text" name="name" onChange={handleInputChange} />

      <label>Image Link:</label>
      <input type="text" name="image" onChange={handleInputChange} />

      <label>Ingredients: (seperated by ,)</label>
      <textarea name="ingredients" onChange={handleInputChange}></textarea>
      <label>Steps: (seperated by ,)</label>
      <textarea name="steps" onChange={handleInputChange}></textarea>
      <input type="button" value="Create" onClick={handleCreate} />
    </form>
  )
}

const Editablerecipe = (props) => {
  const [recipe, setRecipe] = useState({
    id: props.recipe.id,
    name: props.recipe.name,
    image: props.recipe.image,
    ingredients: props.recipe.ingredients.toString(),
    steps: props.recipe.steps.toString(),
  })

  const handleUpdate = () => {
    let recipeCopy = { ...recipe }

    recipeCopy.ingredients = recipeCopy.ingredients
      .split(',')
      .map((str) => str.trim())
    recipeCopy.steps = recipeCopy.steps.split(',').map((str) => str.trim())
    console.log(recipeCopy)

    const updatedArr = props.recipes.map((recipe) =>
      recipeCopy.id == recipe.id ? recipeCopy : recipe
    )

    props.setRecipes(updatedArr)

    const request = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(recipeCopy),
    }
    fetch('http://localhost:8080/recipe', request)
      .then((res) => res.text())
      .then((data) => console.log(data))

    props.setEditable(!props.editable)
  }

  const handleInputChange = (e) => {
    setRecipe({ ...recipe, [e.target.name]: e.target.value })
  }

  return (
    <div className="recipe">
      <img src={props.recipe.image} width="300" height="200" />
      <div>
        <input
          type="text"
          defaultValue={props.recipe.name}
          onChange={handleInputChange}
          name="name"
        />
      </div>
      <ul className="ingredients">
        {props.recipe.ingredients.map((ingredient, i) => (
          // <li key={i}>{ingredient}</li>
          <input
            type="text"
            key={i}
            defaultValue={ingredient}
            onChange={handleInputChange}
            name="ingredients"
          />
        ))}
      </ul>
      <ol className="steps">
        {props.recipe.steps.map((step, i) => (
          // <li key={i}>{step}</li>
          <input
            type="text"
            key={i}
            defaultValue={step}
            onChange={handleInputChange}
            name="steps"
          />
        ))}
      </ol>
      <button onClick={props.handleDelete}>Delete</button>
      <button onClick={handleUpdate}>Update</button>
    </div>
  )
}

const Uneditablerecipe = (props) => {
  const handleEdit = () => {
    props.setEditable(!props.editable)
  }

  return (
    <div className="recipe">
      <img src={props.recipe.image} width="300" height="200" />
      <div>{props.recipe.name}</div>
      <ul className="ingredients">
        {props.recipe.ingredients.map((ingredient, i) => (
          <li key={i}>{ingredient}</li>
        ))}
      </ul>
      <ol className="steps">
        {props.recipe.steps.map((step, i) => (
          <li key={i}>{step}</li>
        ))}
      </ol>
      <button onClick={props.handleDelete}>Delete</button>
      <button onClick={handleEdit}>Edit</button>
    </div>
  )
}

const Recipe = (props) => {
  // recipes, setRecipes, recipe
  const [editable, setEditable] = useState(false)

  const handleDelete = () => {
    props.setRecipes(
      props.recipes.filter((recipe) => props.recipe.id !== recipe.id)
    )
    fetch('http://localhost:8080/recipe', {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ id: props.recipe.id }),
    })
      .then((res) => res.text())
      .then((text) => console.log(text))
  }

  return (
    <div>
      {!editable && (
        <Uneditablerecipe
          editable={editable}
          setEditable={setEditable}
          handleDelete={handleDelete}
          {...props}
        />
      )}
      {editable && (
        <Editablerecipe
          editable={editable}
          setEditable={setEditable}
          handleDelete={handleDelete}
          {...props}
        />
      )}
    </div>
  )
}

const App = () => {
  const [recipes, setRecipes] = useState([])

  useEffect(() => {
    console.log(recipes)
  }, [recipes])

  useEffect(() => {
    fetch('http://localhost:8080/recipe')
      .then((res) => res.json())
      .then((data) => setRecipes(data))
  }, [])

  return (
    <div className="App">
      <Create recipes={recipes} setRecipes={setRecipes}></Create>

      <div className="recipes">
        {recipes.map((recipe, i, recipes) => (
          <Recipe
            key={i}
            recipe={recipe}
            recipes={recipes}
            setRecipes={setRecipes}></Recipe>
        ))}
      </div>
    </div>
  )
}

export default App
